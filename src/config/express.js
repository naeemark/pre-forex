const express = require('express');
const bodyParser = require('body-parser');
const cron = require("node-cron");
const cors = require('cors');
const api = require('../../src/api/v1/rates/list');
const { errorMiddleware } = require('../middlewares/error');
const middlewareMonitoring = require('../middlewares/monitoring');

/**
* Express instance
* @public
*/
const app = express();

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// Monitoring
app.use(middlewareMonitoring);

/**
 * Health status
 */
const healthRoute = express.Router();
healthRoute.get('/health', (req, res) => res.send('OK'));
app.use('/', healthRoute);

// enable authentication

// mount api routes
app.use('/api/v1/rates', api);

//************************** Test Scheduler
app.get("/api/v1/users", (req, res) => {
  const durationIndex = (process.env['DURATION_CRON_INDEX']) ? parseInt(process.env['DURATION_CRON_INDEX']) : 0
  const customers = [
    { id: 1, firstName: "John", lastName: "Doe" },
    { id: 2, firstName: "Steve", lastName: "Smith" },
    { id: 3, firstName: "Naeem", lastName: "Arshad" },
    { id: 33, firstName: "Aslam", lastName: "Anwar" },
    { id: 4, firstName: "Mary", lastName: "Swanson", duration: durationMinutes[durationIndex], index: durationIndex }
  ];
  if (durationIndex < durationMinutes.length - 1)
    process.env['DURATION_CRON_INDEX'] = parseInt(durationIndex) + 1
  else
    process.env['DURATION_CRON_INDEX'] = 0
  res.json(customers);
});

const durationMinutes = [1, 2, 3, 5, 10, 15, 30]

// schedule tasks to be run on the server   
console.log(`Running CRON for min(s):${durationMinutes[1]} @ ${new Date()}`);
cron.schedule(`*/${durationMinutes[1]} * * * *`, function () {
  console.log(`Running CRON for min(s):${durationMinutes[1]} @ ${new Date()}`);
});
//************************** Test Scheduler //


// if error is not an instanceOf APIError, convert it.
app.use(errorMiddleware.converter);

// catch 404 and forward to error handler
app.use(errorMiddleware.notFound);

// error handler, send stacktrace only during development
app.use(errorMiddleware.handler);

module.exports = app;
