require('dotenv-safe').config();
const express = require("express");
const cron = require("node-cron");
const cors = require("cors");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const { port, mongo_db_uri } = require('../config/vars');
const startupBoot = require('../boot');
const async = require('async');


const rates = require("../api/v1/rates/list");
// const projects = require("./routes/api/projects");
// const categories = require("./routes/api/categories");
// const allocationRequests = require("./routes/api/allocationRequests");

// Instantiate Express
const app = express();


// BodyParse Middleware initialization
app.use(bodyParser.json());


// Enable CORS
app.use(cors());

// // Use Routes
app.use("/api/v1/rates", rates);
// app.use("/api/projects", projects);
// app.use("/api/categories", categories);
// app.use("/api/allocationRequests", allocationRequests);

app.get("/api/v1/users", (req, res) => {
  const durationIndex = (process.env['DURATION_CRON_INDEX']) ? parseInt(process.env['DURATION_CRON_INDEX']) : 0
  const customers = [
    { id: 1, firstName: "John", lastName: "Doe" },
    { id: 2, firstName: "Steve", lastName: "Smith" },
    { id: 3, firstName: "Naeem", lastName: "Arshad" },
    { id: 4, firstName: "Mary", lastName: "Swanson", duration: durationMinutes[durationIndex], index: durationIndex }
  ];
  if (durationIndex < durationMinutes.length - 1)
    process.env['DURATION_CRON_INDEX'] = parseInt(durationIndex) + 1
  else
    process.env['DURATION_CRON_INDEX'] = 0
  res.json(customers);
});

const durationMinutes = [1, 2, 3, 5, 10, 15, 30]

// schedule tasks to be run on the server   
console.log(`Running CRON for min(s):${durationMinutes[1]} @ ${new Date()}`);
cron.schedule(`*/${durationMinutes[1]} * * * *`, function () {
  console.log(`Running CRON for min(s):${durationMinutes[1]} @ ${new Date()}`);
});

// Starting server
// app.listen(port, () => console.log(`Server started on port: ${port}`));
const startupTasks = [];
startupBoot.forEach((boot) => {
  startupTasks.push(async.apply(boot, app));
});

async.waterfall(startupTasks, (err) => {
  if (err) {
    logger.error('Unable to start server - please restart the service', err);
    process.exit(1);
  }
});
module.exports = app;
