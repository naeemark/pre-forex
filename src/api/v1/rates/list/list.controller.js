const httpStatus = require('http-status');
const { listRate } = require('../../../../services/rate');

/**
 * users
 * @public
 */
exports.list = async (req, res, next) => {
  res.status(httpStatus.OK);
  const response = await listRate();
  return res.json({
    responseCode: httpStatus.OK,
    responseMessage: 'OK',
    response: response
  });
};
